#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>

TAR=../libjcalendar-java_$2.orig.tar.gz
DIR=libjcalendar-java-$2.orig

# clean up the upstream tarball
unzip $3 -d $DIR
tar -c -z -f $TAR --exclude '*/doc/api/*' --exclude '*.jar' $DIR
rm -rf $DIR $3

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir
  echo "moved $TAR to $origDir"
fi

exit 0
